#!/usr/bin/env python3

import rospy
import pysurvive
import sys

from geometry_msgs.msg import Pose

if __name__=='__main__':
    actx = pysurvive.SimpleContext(sys.argv)
    rospy.init_node('pose_sensor',anonymous=True)
    pub = rospy.Publisher('/pose/htcvive',Pose,queue_size=1) 
    rate = rospy.Rate(50)
    while not rospy.is_shutdown():
        if actx.Running():
            updated = actx.NextUpdated()
            if updated is not None:
                p = Pose()
                u = updated.Pose()[0]
                p.position.x = u.Pos[0]
                p.position.y = u.Pos[1]
                p.position.z = u.Pos[2]
                p.orientation.w = u.Rot[0]
                p.orientation.x = u.Rot[1]
                p.orientation.y = u.Rot[2]
                p.orientation.z = u.Rot[3]
                pub.publish(p)
                rate.sleep()	

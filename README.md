# F1T

Usage of libsurvive library to calculate the exact position of the Vive Tracker.\
Using Base Stations 2.0.\
Tested on Ubuntu 18.04.4.\
Working with libsruvive commit `c2e6c7b69ad05751580ea63d2dcb1c50cc348587` with adjusted `src/` folder as shown in commit change `3a53ecb66294f5a68c2dc0ad690e331f8f9eddc1`.

## Set up

1.  Download aditional packages: (libUSB, pthread, libX11, zlib, liblapack, libopenblas, libgd-dev).
2.  Clone git repository [libsurvive][5].
3.  Move file 81-vive.rules (`/useful_files/`) to `/etc/udev/rules.d/` and reboot.
4.  Power on Base stations, attach the dongle to the cradle and connect the cradle with the USB port, power up the tracker and it should automatically connect to the dongle.
5.  If you are using Nvidia card, allow HMD in `/etc/X11/xorg.conf`.
6.  `cd libsurvive && sudo make`.
7.  Calibrate, you can use command `./bin/survive-cli`. Location of calibration `config.json` file can be found on the first line of output of command `./bin/api_example --v 100`. 

Calibration can take even minutes, depends on number of Base Station and on the external conditions. After every adjustment of Base station (lighthouse), delete `config.json` and calibrate once more.\
Command for installing packages:
*  `sudo apt-get install libusb-1.0-0-dev liblapacke-dev libopenblas-dev libatlas-base-dev lib1g-dev libx11-dev libpthread-stubs0-dev libgd-dev`

### Setting up multiple Base stations
Every single Base station has to be running on a different channel. There are all in all 16 channels, so in theory it is possible to have 16 functional Base stations. To change channels use a paperclip to gently push the button in the hole at the back of the sensor, it is just like a reset button in other hardware devices. Every push of a button increments a channel by one. You can check channels of connected Base Stations in SteamVR. It is quite simple, should you have a headset. If you do not have a headset, just a tracker, it is necessary to tweak Steam VR a bit. This will make the app not working, but it will show channels right away.

#### Checking channels without headset

1.	Download Steam and log in.
2.	Download SteamVR with opt in for Beta.
3.	Locate Steam install folder and in it find `/steamapps/common/SteamVR/drivers/null/resources/settings/default.vrsettings` and open it with text editor.
4.	Change "enable" to true and save the file.
5.	Locate `/steamapps/common/SteamVR/resources/settings/default.vrsettings` and open with text editor.
6.	Search for "requireHmd" and set its value to false.
7.	Search for "forcedDriver" and set its value to "null".
8.	Search for "activateMultipleDrivers" and set its value to true.
9.	Save and close the file.
10.	When you have all Base Stations connected as well as the Tracker, turn on SteamVR and hover over the green icon of a Base Station, channel should be visible.
11. For the app to be working properly, you have to revert all those changes.

## Visualization using websocketd

1.  Download [Websocketd][6]
2.  Extract and move executable file to the `bin/` folder of libsurvive.
3.  Command: `./bin/survive-websocketd & xdg-open ./tools/viz/index.html`.

## Python bindings

1.	Install Python3 (tested on Python 3.8.5).
2.	Install packages `wheel setuptools`,`ctypesgen` and `cmake` ideally version `3.18.2` or higher.
3.	Run `sudo python3 setup.py install`.
4.	Copy `pose.py` to the `libsurvive` folder and run `python3 pose.py`

If you have installed Python 2, some generated files can generate differently. If so, rewrite all `sys.maxint` to `sys.maxsize` in `bindings/python/pysurvive/pysurvive_generated.py`.

## Experiments

Experiment with one Base station is detailed in `Documentation/` folder. For running scripts to get graphs, at least [gnuplot 5.4][4] must be installed. Run script using command `./plot_136.gp` to get the graph showing field of vision of the base station mounted 136 cm from the ground, so on for 170 and 195 cm.

Comparison between HTCVIVE and cartographer can be found in `Documentation/`. Run the python3 scripts, make sure you have `matplotlib` installed.

## Current bugs

Reflective floors or bright lights have negative impacts on calibration.\
If calibrated, but high reflectivity, the program will recalibrate during run time and reset the initial coordinates.\
Visualization using websocketd can pick up high latency.\
Sometimes created files are owned by root, if needed use `sudo chown -R <user> <file>`.\
Flag `--force-calibrate` does not currently work, manually delete `config.json`.\
Fails to work reliably if the Tracker is moving fast.

## References

- https://github.com/cntools/libsurvive - original libsurvive library, the beginning of this work

- http://websocketd.com/ - used for visualization

# Contribute

Use [OneFlow][1] branching model and keep the [changelog][2].

Write [great git commit messages][3]:

1. Separate subject from body with a blank line.
2. Limit the subject line to 50 characters.
3. Capitalize the subject line.
4. Do not end the subject line with a period.
5. Use the imperative mood in the subject line.
6. Wrap the body at 72 characters.
7. Use the body to explain what and why vs. how.

[1]: https://www.endoflineblog.com/oneflow-a-git-branching-model-and-workflow
[2]: ./CHANGELOG.md
[3]: https://chris.beams.io/posts/git-commit/
[4]: https://sourceforge.net/projects/gnuplot/files/gnuplot/5.4.0/
[5]: https://github.com/cntools/libsurvive
[6]: http://websocketd.com/

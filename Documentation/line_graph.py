import matplotlib.pyplot as plt
import copy

raw_cartographer_trajectory = []
raw_lib_trajectory = []
f1 = open("straight_movement_data/line_cart_data.txt", "r")
for line in f1:
	raw_cartographer_trajectory.append(float(line))
cartographer_trajectory = []	
for i in range(0,len(raw_cartographer_trajectory),2):
	cartographer_trajectory.append([raw_cartographer_trajectory[i],raw_cartographer_trajectory[i+1]])
first_cartographer_point = copy.deepcopy(cartographer_trajectory[0])
for i in range(len(cartographer_trajectory)):
	cartographer_trajectory[i][0] -= first_cartographer_point[0]
	cartographer_trajectory[i][1] -= first_cartographer_point[1]
	print(cartographer_trajectory[i])

	
x_cartographer_val = [x[0] for x in cartographer_trajectory]
y_cartographer_val = [x[1] for x in cartographer_trajectory]

f1.close()
print("f1 closed")
f2 = open("straight_movement_data/line_lib_data.txt","r")
for line in f2:
	raw_lib_trajectory.append(float(line))
lib_trajectory = []
for i in range(0,len(raw_lib_trajectory),2):
	lib_trajectory.append([raw_lib_trajectory[i],raw_lib_trajectory[i+1]])
first_lib_point = copy.deepcopy(lib_trajectory[0])
for i in range(len(lib_trajectory)):
	lib_trajectory[i][0] -= first_lib_point[0]
	lib_trajectory[i][1] -= first_lib_point[1]
	print(lib_trajectory[i])

f2.close()	
x_lib_val = [x[0] for x in lib_trajectory]
y_lib_val = [x[1] for x in lib_trajectory]
plt.plot(x_cartographer_val,y_cartographer_val,'bo',label='Cartographer data',markersize = 1)
plt.plot(x_lib_val,y_lib_val,'ro',label = 'HTCVIVE data',markersize = 1)
plt.legend()
plt.title('Tracking moving vehicle in a line')
plt.xlabel('x [m]')
plt.ylabel('y [m]')
plt.grid()
plt.show()

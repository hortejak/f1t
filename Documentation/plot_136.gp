#!/usr/local/bin/gnuplot -persist
#
#    
#    	G N U P L O T
#    	Version 5.4 patchlevel 0    last modified 2020-07-13 
#    
#    	Copyright (C) 1986-1993, 1998, 2004, 2007-2020
#    	Thomas Williams, Colin Kelley and many others
#    
#    	gnuplot home:     http://www.gnuplot.info
#    	faq, bugs, etc:   type "help FAQ"
#    	immediate help:   type "help"  (plot window: hit 'h')
set terminal png truecolor nocrop enhanced size 1680,1050 font "arial,12.0" 
set output 'test136.png'
unset clip points
set clip one
unset clip two
unset clip radial
set errorbars front 1.000000 
unset border
set zdata 
set ydata 
set xdata 
set y2data 
set x2data 
set boxwidth
set boxdepth 0
set style fill  empty border
set style rectangle back fc  bgnd fillstyle   solid 1.00 border lt -1
set style circle radius graph 0.02 
set style ellipse size graph 0.05, 0.03 angle 0 units xy
set dummy x, y
set format x "% h" 
set format y "% h" 
set format x2 "% h" 
set format y2 "% h" 
set format z "% h" 
set format cb "% h" 
set format r "% h" 
set ttics format "% h"
set timefmt "%d/%m/%y,%H:%M"
set angles radians
set tics back
set grid nopolar
set grid noxtics nomxtics noytics nomytics noztics nomztics nortics nomrtics \
 nox2tics nomx2tics noy2tics nomy2tics nocbtics nomcbtics
set grid layerdefault   linecolor rgb "grey"  linewidth 1.000 dashtype solid,  linecolor rgb "grey"  linewidth 1.000 dashtype solid
unset raxis
set theta counterclockwise right
set style parallel front  lt black linewidth 2.000 dashtype solid
set key notitle
#set key tmargin right vertical Right noreverse enhanced noautotitle nobox
set key fixed right top vertical Right noreverse enhanced noautotitle nobox
set key noinvert samplen 7 spacing 1 width 2 height 0
set key maxcolumns 0 maxrows 0
set key noopaque
unset label
unset arrow
unset style line
unset style arrow
set style histogram clustered gap 2 title textcolor lt -1
unset object
unset walls
set style textbox  transparent margins  1.0,  1.0 border  lt -1 linewidth  1.0
set offsets 0, 0, 0, 0
set pointsize 1
set pointintervalbox 1
set encoding utf8
unset polar
unset parametric
set spiderplot
set style spiderplot  linewidth 2.000 dashtype solid pointtype 1 pointsize default
set style spiderplot fillstyle  transparent solid 0.10 border
set grid spider linecolor rgb "grey"  linewidth 1.000 dashtype solid,  linecolor rgb "grey"  linewidth 1.000 dashtype solid
unset decimalsign
unset micro
unset minussign
set view 60, 30, 1, 1
set view azimuth 0
set rgbmax 255
set samples 100, 100
set isosamples 10, 10
set surface 
unset contour
set cntrlabel  format '%8.3g' font '' start 5 interval 20
set mapping cartesian
set datafile separator whitespace
set datafile nocolumnheaders
unset hidden3d
set cntrparam order 4
set cntrparam linear
set cntrparam levels 5
set cntrparam levels auto
set cntrparam firstlinetype 0 unsorted
set cntrparam points 5
set size ratio 1 1,1
set origin 0,0
set style data spiderplot
set style function lines
unset xzeroaxis
unset yzeroaxis
unset zzeroaxis
unset x2zeroaxis
unset y2zeroaxis
set xyplane relative 0.5
set tics scale  1, 0.5, 1, 1, 1
set mxtics default
set mytics default
set mztics default
set mx2tics default
set my2tics default
set mcbtics default
set mrtics default
set nomttics
unset xtics
unset ytics
unset ztics
unset x2tics
unset y2tics
unset cbtics
unset rtics
unset ttics
set paxis 1 tics axis in scale 1,0.5 nomirror norotate  autojustify
set paxis 1 tics  norangelimit 200 font ".8"
unset paxis 7 tics
unset paxis 8 tics
unset paxis 9 tics
unset paxis 10 tics
unset paxis 11 tics
unset paxis 12 tics
unset paxis 13 tics
unset paxis 14 tics
unset paxis 15 tics
unset paxis 16 tics
unset paxis 17 tics
unset paxis 18 tics
unset paxis 19 tics
unset paxis 20 tics
unset paxis 21 tics
unset paxis 22 tics
unset paxis 23 tics
unset paxis 24 tics
unset paxis 25 tics
unset paxis 26 tics
unset paxis 27 tics
unset paxis 28 tics
unset paxis 29 tics
unset paxis 30 tics
unset paxis 31 tics
unset paxis 32 tics
unset paxis 33 tics
unset paxis 34 tics
unset paxis 35 tics
unset paxis 36 tics
unset paxis 37 tics
unset paxis 38 tics
unset paxis 39 tics
unset paxis 40 tics
unset paxis 41 tics
unset paxis 42 tics
unset paxis 43 tics
unset paxis 44 tics
unset paxis 45 tics
unset paxis 46 tics
unset paxis 47 tics
unset paxis 48 tics
unset paxis 49 tics
unset paxis 50 tics
unset paxis 51 tics
unset paxis 52 tics
unset paxis 53 tics
unset paxis 54 tics
unset paxis 55 tics
unset paxis 56 tics
unset paxis 57 tics
unset paxis 58 tics
unset paxis 59 tics
unset paxis 60 tics
unset paxis 61 tics
unset paxis 62 tics
unset paxis 63 tics
unset paxis 64 tics
unset paxis 65 tics
unset paxis 66 tics
unset paxis 67 tics
unset paxis 68 tics
unset paxis 69 tics
unset paxis 70 tics
unset paxis 71 tics
unset paxis 72 tics
unset paxis 73 tics
unset paxis 74 tics
unset paxis 75 tics
unset paxis 76 tics
unset paxis 77 tics
unset paxis 78 tics
unset paxis 79 tics
unset paxis 80 tics
unset paxis 81 tics
unset paxis 82 tics
unset paxis 83 tics
unset paxis 84 tics
unset paxis 85 tics
unset paxis 86 tics
unset paxis 87 tics
unset paxis 88 tics
unset paxis 89 tics
unset paxis 90 tics
unset paxis 91 tics
unset paxis 92 tics
unset paxis 93 tics
unset paxis 94 tics
unset paxis 95 tics
unset paxis 96 tics
unset paxis 97 tics
unset paxis 98 tics
unset paxis 99 tics
unset paxis 100 tics
set title "Height of 136 cm" 
set title  offset character -50, 0, 0 font "Times,40" textcolor lt -1 norotate
set timestamp bottom 
set timestamp "" 
set timestamp  font "" textcolor lt -1 norotate
set trange [ * : * ] noreverse nowriteback
set urange [ * : * ] noreverse nowriteback
set vrange [ * : * ] noreverse nowriteback
set xlabel "" 
set xlabel  font "" textcolor lt -1 norotate
set x2label "" 
set x2label  font "" textcolor lt -1 norotate
set xrange [ * : * ] noreverse writeback
set x2range [ * : * ] noreverse writeback
set ylabel "" 
set ylabel  font "" textcolor lt -1 rotate
set y2label "" 
set y2label  font "" textcolor lt -1 rotate
set yrange [ * : * ] noreverse writeback
set y2range [ * : * ] noreverse writeback
set zlabel "" 
set zlabel  font "" textcolor lt -1 norotate
set zrange [ * : * ] noreverse writeback
set cblabel "" 
set cblabel  font "" textcolor lt -1 rotate
set cbrange [ * : * ] noreverse writeback
set rlabel "" 
set rlabel  font "" textcolor lt -1 norotate
set rrange [ * : * ] noreverse writeback
set paxis 1 range [ 0.00000 : 1200.00 ]  noextend
set paxis 1 label "MAX" 
set paxis 1 label  font "" textcolor lt -1 norotate
set paxis 2 range [ 0.00000 : 1200.00 ]  noextend
set paxis 2 label "MIN" 
set paxis 2 label  font "" textcolor lt -1 norotate
set paxis 3 range [ 0.00000 : 1200.00 ]  noextend
set paxis 3 label "W50" 
set paxis 3 label  font "" textcolor lt -1 norotate
set paxis 4 range [ 0.00000 : 1200.00 ]  noextend
set paxis 4 label "W100" 
set paxis 4 label  font "" textcolor lt -1 norotate
set paxis 5 range [ 0.00000 : 1200.00 ]  noextend
set paxis 5 label "W150" 
set paxis 5 label  font "" textcolor lt -1 norotate
set paxis 6 range [ 0.00000 : 1200.00 ]  noextend
set paxis 6 label "W300" 
set paxis 6 label  font "" textcolor lt -1 norotate
set paxis 7 range [ 0.00000 : 1200.00 ]  noextend
set paxis 8 range [ 0.00000 : 1200.00 ]  noextend
set paxis 9 range [ 0.00000 : 1200.00 ]  noextend
set paxis 10 range [ 0.00000 : 1200.00 ]  noextend
set paxis 11 range [ 0.00000 : 1200.00 ]  noextend
set paxis 12 range [ 0.00000 : 1200.00 ]  noextend
set paxis 13 range [ 0.00000 : 1200.00 ]  noextend
set paxis 14 range [ 0.00000 : 1200.00 ]  noextend
set paxis 15 range [ 0.00000 : 1200.00 ]  noextend
set paxis 16 range [ 0.00000 : 1200.00 ]  noextend
set paxis 17 range [ 0.00000 : 1200.00 ]  noextend
set paxis 18 range [ 0.00000 : 1200.00 ]  noextend
set paxis 19 range [ 0.00000 : 1200.00 ]  noextend
set paxis 20 range [ 0.00000 : 1200.00 ]  noextend
set paxis 21 range [ 0.00000 : 1200.00 ]  noextend
set paxis 22 range [ 0.00000 : 1200.00 ]  noextend
set paxis 23 range [ 0.00000 : 1200.00 ]  noextend
set paxis 24 range [ 0.00000 : 1200.00 ]  noextend
set paxis 25 range [ 0.00000 : 1200.00 ]  noextend
set paxis 26 range [ 0.00000 : 1200.00 ]  noextend
set paxis 27 range [ 0.00000 : 1200.00 ]  noextend
set paxis 28 range [ 0.00000 : 1200.00 ]  noextend
set paxis 29 range [ 0.00000 : 1200.00 ]  noextend
set paxis 30 range [ 0.00000 : 1200.00 ]  noextend
set paxis 31 range [ 0.00000 : 1200.00 ]  noextend
set paxis 32 range [ 0.00000 : 1200.00 ]  noextend
set paxis 33 range [ 0.00000 : 1200.00 ]  noextend
set paxis 34 range [ 0.00000 : 1200.00 ]  noextend
set paxis 35 range [ 0.00000 : 1200.00 ]  noextend
set paxis 36 range [ 0.00000 : 1200.00 ]  noextend
set paxis 37 range [ 0.00000 : 1200.00 ]  noextend
set paxis 38 range [ 0.00000 : 1200.00 ]  noextend
set paxis 39 range [ 0.00000 : 1200.00 ]  noextend
set paxis 40 range [ 0.00000 : 1200.00 ]  noextend
set paxis 41 range [ 0.00000 : 1200.00 ]  noextend
set paxis 42 range [ 0.00000 : 1200.00 ]  noextend
set paxis 43 range [ 0.00000 : 1200.00 ]  noextend
set paxis 44 range [ 0.00000 : 1200.00 ]  noextend
set paxis 45 range [ 0.00000 : 1200.00 ]  noextend
set paxis 46 range [ 0.00000 : 1200.00 ]  noextend
set paxis 47 range [ 0.00000 : 1200.00 ]  noextend
set paxis 48 range [ 0.00000 : 1200.00 ]  noextend
set paxis 49 range [ 0.00000 : 1200.00 ]  noextend
set paxis 50 range [ 0.00000 : 1200.00 ]  noextend
set paxis 51 range [ 0.00000 : 1200.00 ]  noextend
set paxis 52 range [ 0.00000 : 1200.00 ]  noextend
set paxis 53 range [ 0.00000 : 1200.00 ]  noextend
set paxis 54 range [ 0.00000 : 1200.00 ]  noextend
set paxis 55 range [ 0.00000 : 1200.00 ]  noextend
set paxis 56 range [ 0.00000 : 1200.00 ]  noextend
set paxis 57 range [ 0.00000 : 1200.00 ]  noextend
set paxis 58 range [ 0.00000 : 1200.00 ]  noextend
set paxis 59 range [ 0.00000 : 1200.00 ]  noextend
set paxis 60 range [ 0.00000 : 1200.00 ]  noextend
set paxis 61 range [ 0.00000 : 1200.00 ]  noextend
set paxis 62 range [ 0.00000 : 1200.00 ]  noextend
set paxis 63 range [ 0.00000 : 1200.00 ]  noextend
set paxis 64 range [ 0.00000 : 1200.00 ]  noextend
set paxis 65 range [ 0.00000 : 1200.00 ]  noextend
set paxis 66 range [ 0.00000 : 1200.00 ]  noextend
set paxis 67 range [ 0.00000 : 1200.00 ]  noextend
set paxis 68 range [ 0.00000 : 1200.00 ]  noextend
set paxis 69 range [ 0.00000 : 1200.00 ]  noextend
set paxis 70 range [ 0.00000 : 1200.00 ]  noextend
set paxis 71 range [ 0.00000 : 1200.00 ]  noextend
set paxis 72 range [ 0.00000 : 1200.00 ]  noextend
set paxis 73 range [ 0.00000 : 1200.00 ]  noextend
set paxis 74 range [ 0.00000 : 1200.00 ]  noextend
set paxis 75 range [ 0.00000 : 1200.00 ]  noextend
set paxis 76 range [ 0.00000 : 1200.00 ]  noextend
set paxis 77 range [ 0.00000 : 1200.00 ]  noextend
set paxis 78 range [ 0.00000 : 1200.00 ]  noextend
set paxis 79 range [ 0.00000 : 1200.00 ]  noextend
set paxis 80 range [ 0.00000 : 1200.00 ]  noextend
set paxis 81 range [ 0.00000 : 1200.00 ]  noextend
set paxis 82 range [ 0.00000 : 1200.00 ]  noextend
set paxis 83 range [ 0.00000 : 1200.00 ]  noextend
set paxis 84 range [ 0.00000 : 1200.00 ]  noextend
set paxis 85 range [ 0.00000 : 1200.00 ]  noextend
set paxis 86 range [ 0.00000 : 1200.00 ]  noextend
set paxis 87 range [ 0.00000 : 1200.00 ]  noextend
set paxis 88 range [ 0.00000 : 1200.00 ]  noextend
set paxis 89 range [ 0.00000 : 1200.00 ]  noextend
set paxis 90 range [ 0.00000 : 1200.00 ]  noextend
set paxis 91 range [ 0.00000 : 1200.00 ]  noextend
set paxis 92 range [ 0.00000 : 1200.00 ]  noextend
set paxis 93 range [ 0.00000 : 1200.00 ]  noextend
set paxis 94 range [ 0.00000 : 1200.00 ]  noextend
set paxis 95 range [ 0.00000 : 1200.00 ]  noextend
set paxis 96 range [ 0.00000 : 1200.00 ]  noextend
set paxis 97 range [ 0.00000 : 1200.00 ]  noextend
set paxis 98 range [ 0.00000 : 1200.00 ]  noextend
set paxis 99 range [ 0.00000 : 1200.00 ]  noextend
set paxis 100 range [ 0.00000 : 1200.00 ]  noextend
unset logscale
unset jitter
set zero 1e-08
set lmargin  -1
set bmargin  -1
set rmargin  -1
set tmargin  -1
set locale "cs_CZ.UTF-8"
set pm3d explicit at s
set pm3d scansautomatic
set pm3d interpolate 1,1 flush begin noftriangles noborder corners2color mean
set pm3d clip z 
set pm3d nolighting
set palette positive nops_allcF maxcolors 0 gamma 1.5 color model RGB 
set palette rgbformulae 7, 5, 15
set colorbox default
set colorbox vertical origin screen 0.9, 0.2 size screen 0.05, 0.6 front  noinvert bdefault
set style boxplot candles range  1.50 outliers pt 7 separation 1 labels auto unsorted
set loadpath 
set fontpath
set psdir
set fit brief errorvariables nocovariancevariables errorscaling prescale nowrap v5
GNUTERM = "png"
VoxelDistance = 9.00500479207635e-308
array Array1[6] = [620,80,0,150,230,510]
array Array2[6] = [620,50,0,180,350,850]
array Array3[6] = [680,30,80,230,420,980]
array Array4[6] = [680,15,120,340,520,1080]
array Array5[6] = [680,0,200,340,520,1140]
array Array6[6] = [640,0,300,460,620,640]
## Last datafile plotted: "data.txt"
#plot for [i=1:6] "data.txt" using i title columnhead
plot      keyentry with spiderplot lc 1 lw 2 title "0°",      for [i=1:|Array1|] Array1 using (Array1[i]) lc 1 lw 2, \
newspiderplot,      keyentry with spiderplot lc 2 lw 2 title "10°",      for [j=1:|Array2|] Array2 using (Array2[j]) lc 2 lw 2 notitle, \
newspiderplot,		keyentry with spiderplot lc 3 lw 2 title "20°",      for [j=1:|Array3|] Array3 using (Array3[j]) lc 3 lw 2 notitle, \
newspiderplot,		keyentry with spiderplot lc 4 lw 2 title "30°",      for [j=1:|Array4|] Array4 using (Array4[j]) lc 4 lw 2 notitle, \
newspiderplot,		keyentry with spiderplot lc 5 lw 2 title "40°",      for [j=1:|Array5|] Array5 using (Array5[j]) lc 5 lw 2 notitle, \
newspiderplot,		keyentry with spiderplot lc 6 lw 2 title "50°",      for [j=1:|Array6|] Array6 using (Array6[j]) lc 6 lw 2 notitle, \
#    EOF

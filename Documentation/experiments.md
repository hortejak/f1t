# Experiments

## Experiments with 1 Base Station

The goal of this experiment was to determine the ideal position of the Base Station (later just LH) and its properties.

#### Prerequisities:
 - HTC Base Station 2.0
 - HTC VIVE Tracker (2018) with a dongle
 - set up environment, described in detail in `README.md`

#### Procedure:
 - Adjust the LH to certain angle and height, calibrate and test the maximum distance in which LH detects the Tracker in certain directions.
 - Repeat for different angles and heights.


For this experiment the default height of the Tracker was determined to be 22 cm, which should correspond with the height in which the Tracker will be mounted on the F1/10 car. The Base Station was mounted in 3 different heights (136 cm, 170 cm and  195 cm). The sensor angle is a angle between main axis of the sensor and the axis of the pole.
Targets of this measurement were the minimum and maximum distance in a straight line and the width of the Base Station's view in certain distances (50 cm, 100 cm, 150 cm, 300 cm).
All of this data were acquired using MPFIT poser and StateBased disambiguator. Measured values may slightly vary due to errors in calibration and inaccurate setting of the sensor angle.
Data at certain heights are situated in tables below.

#### 136 cm
|Base Station Angle [°]|Minimum distance [cm]|Maximum Distance [cm]|Width at 50 cm [cm]|Width at 100 cm [cm]|Width at 150 cm [cm]|Width at 300 cm [cm]|
|:---:|:---:|:---:|:---:|:---:|:---:|:---:|
|0|80|620|0|150|230|510|
|10|50|620|0|180|350|850|
|20|30|680|80|230|420|980|
|30|15|680|120|340|520|1080|
|40|X|680|200|340|520|1140|
|50|X|640|300|460|620|640|

#### 170 cm
|Base Station Angle [°]|Minimum distance [cm]|Maximum Distance [cm]|Width at 50 cm [cm]|Width at 100 cm [cm]|Width at 150 cm [cm]|Width at 300 cm [cm]|
|:---:|:---:|:---:|:---:|:---:|:---:|:---:|
|0|95|630|0|30|220|680|
|10|60|630|0|170|350|820|
|20|40|720|0|210|380|970|
|30|20|680|140|310|480|1120|
|40|X|680|190|300|410|880|
|50|X|670|340|400|680|720|

#### 195 cm
|Base Station Angle [°]|Minimum distance [cm]|Maximum Distance [cm]|Width at 50 cm [cm]|Width at 100 cm [cm]|Width at 150 cm [cm]|Width at 300 cm [cm]|
|:---:|:---:|:---:|:---:|:---:|:---:|:---:|
|0|115|640|0|0|190|640|
|10|70|640|0|100|200|660|
|20|50|720|0|250|460|1060|
|30|30|700|160|340|560|1120|
|40|X|680|210|390|580|1140|
|50|X|660|340|500|660|780|

#### Conclusion
From data above it is perfectly visible, that the angle of the Base Station should not be lower than 20° and higher than 40°. As for the height, less than 170 cm is not recommended, not only there is a higher risk of signal interruption due to view blocks, the field of view is grately limited too, at higher distances the position recognition becomes jittery. The main advantage of this height setting is in the minimum distance it recognizes the Tracker, but the little extra bonus on short distances does not outweight limitations at greater distances. For the tracking of the F1/10 car, it is best to use the maximum height of the sensor, lower it only in case, when recognition in shorter distances is necessary. With those initial settings applied, the maximum recognition distance is settled at 700 cm, with the field of view of ruffly 120°.

## Comparison between HTC VIVE and Cartographer

The goal of this experiment is to compare the accuracy and reliability of HTC VIVE compared to the standard cartographer.

### Stationary Tracker

#### Procedure

 - Start cartographer and libsurvive
 - Let the program save 15 sec of data (Pose)
 - Draw a 2D graph showing the position of the Tracker
 
#### Conclusion

From the graph it is perfectly visible, that HTC VIVE outclasses the cartographer by a long shot. The dispersion of the cartographer can even reach out to 6 mm, which is way more than HTC VIVE`s 0.2 mm.

### Movement in a straight line

#### Procedure

 - Start a cartographer and libsurvive
 - Drive slowly in a line approximately 4 m
 - Repeat for normal and fast velocity
 - Draw a 2D graph showing the position of the Tracker
 
#### Conclusion

The line for the slow movement is much narrower for the HTC VIVE, so in this regard, HTC VIVE is superior. This cannot be said for faster movements, where HTC VIVE suffers a bit. For the fast movement the HTC VIVE failed considerably.

### Movement in a circle

#### Procedure

 - Start a cartographer and libsurvive
 - Drive slowly in a circle around a column
 - Draw a 2d graph showing the position of the Tracker

#### Conclusion

Both circles seem to be very much the same, HTC VIVE sends less data, so the circle consists of lesser number of dots.

import matplotlib.pyplot as plt

raw_cartographer_trajectory = []
raw_lib_trajectory = []
cartographer_x_sum = 0
cartographer_y_sum = 0
lib_x_sum = 0
lib_y_sum = 0
f1 = open("stationary_data/stationary_cart_data.txt", "r")
for line in f1:
	raw_cartographer_trajectory.append(float(line))
cartographer_trajectory = []	
for i in range(0,len(raw_cartographer_trajectory),2):
	cartographer_trajectory.append([raw_cartographer_trajectory[i],raw_cartographer_trajectory[i+1]])

for i in range(len(cartographer_trajectory)):
	cartographer_x_sum += cartographer_trajectory[i][0]
	cartographer_y_sum += cartographer_trajectory[i][1]
	print(cartographer_trajectory[i])
	
cartographer_average = (cartographer_x_sum/len(cartographer_trajectory),cartographer_y_sum/len(cartographer_trajectory))
for i in range(len(cartographer_trajectory)):
	cartographer_trajectory[i][0] -=cartographer_average[0]
	cartographer_trajectory[i][1] -=cartographer_average[1]

print("THIS IS CARTOGRAPHER AVERAGE")
print(cartographer_average)
	
x_cartographer_val = [x[0] for x in cartographer_trajectory]
y_cartographer_val = [x[1] for x in cartographer_trajectory]

f1.close()
print("f1 closed")
f2 = open("stationary_data/stationary_lib_data.txt","r")
for line in f2:
	raw_lib_trajectory.append(float(line))
lib_trajectory = []
for i in range(0,len(raw_lib_trajectory),2):
	lib_trajectory.append([raw_lib_trajectory[i],raw_lib_trajectory[i+1]])
for i in range(len(lib_trajectory)):
	lib_x_sum += lib_trajectory[i][0]
	lib_y_sum += lib_trajectory[i][1]
	print(lib_trajectory[i])

lib_average = (lib_x_sum/len(lib_trajectory),lib_y_sum/len(lib_trajectory))
for i in range(len(lib_trajectory)):
	lib_trajectory[i][0] -=lib_average[0]
	lib_trajectory[i][1] -=lib_average[1]
	
x_lib_val = [x[0] for x in lib_trajectory]
y_lib_val = [x[1] for x in lib_trajectory]
#the same for data from libsurvive, different plot or translacni matrix
#plt.plot(x_cartographer_val,y_cartographer_val,'bo',x_lib_val,y_lib_val,'ro',markersize=1)
plt.plot(x_cartographer_val,y_cartographer_val,'bo',label='Cartographer data',markersize = 1)
plt.plot(x_lib_val,y_lib_val,'ro',label = 'HTCVIVE data',markersize = 1)
plt.legend()
plt.title('Tracking stationary vehicle')
plt.xlabel('x [m]')
plt.ylabel('y [m]')
plt.grid()
plt.show()

import pysurvive
import sys

actx = pysurvive.SimpleContext(sys.argv)

for obj in actx.Objects():
    print(obj.Name())
    
while actx.Running():
    updated = actx.NextUpdated()
    if updated:
        x_pos = updated.Pose()[0].Pos[0]
        y_pos = updated.Pose()[0].Pos[1]
        z_pos = updated.Pose()[0].Pos[2]
        x_rot = updated.Pose()[0].Rot[0]
        y_rot = updated.Pose()[0].Rot[1]
        z_rot = updated.Pose()[0].Rot[2]
        w_rot = updated.Pose()[0].Rot[3]
        print(updated.Name(), x_pos,y_pos,z_pos,x_rot,y_rot,z_rot,w_rot)

